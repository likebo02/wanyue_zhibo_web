<?php

namespace app\admin\controller\live;

use app\admin\controller\AuthController;
use think\Request;
use think\facade\Route as Url;
use app\admin\model\live\Report as ClassModel;
use wanyue\services\{FormBuilder as Form, JsonService as Json, UtilService as Util};

/**
 * 直播举报分类
 * Class StoreCategory
 * @package app\admin\controller\system
 */
class Report extends AuthController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    /*
     *  异步获取分类列表
     *  @return json
     */
    public function getlist()
    {
        $where = Util::getMore([
            ['uid', 0],
            ['touid', 0],
            ['page', 1],
            ['limit', 20],
            ['order', '']
        ]);
        return Json::successlayui(ClassModel::getList($where));
    }



    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function setstatus(Request $request, $id)
    {

        $data['status']=1;
        ClassModel::edit($data, $id);
        return Json::successful('修改成功!');
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if (!ClassModel::delid($id))
            return Json::fail(ClassModel::getErrorInfo('删除失败,请稍候再试!'));
        else
            return Json::successful('删除成功!');
    }
}
