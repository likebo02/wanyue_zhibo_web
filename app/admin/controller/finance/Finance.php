<?php
/**
 * Created by PhpStorm.
 * User: xurongyao <763569752@qq.com>
 * Date: 2018/6/14 下午5:25
 */

namespace app\admin\controller\finance;

use app\admin\controller\AuthController;
use app\admin\model\user\{User,UserBill};
use app\admin\model\finance\FinanceModel;
use wanyue\services\{UtilService as Util,JsonService as Json};

/**
 * 微信充值记录
 * Class UserRecharge
 * @package app\admin\controller\user
 */
class Finance extends AuthController
{
    /**
     * 显示资金记录
     */
    public function bill()
    {
        $list = UserBill::where('type', 'not in', ['gain', 'system_sub', 'deduction', 'sign'])
            ->where('category', 'not in', 'integral')
            ->field(['title', 'type'])
            ->group('type')
            ->distinct(true)
            ->select()
            ->toArray();
        $this->assign('selectList', $list);
        return $this->fetch();
    }

    /**
     * 显示资金记录ajax列表
     */
    public function billlist()
    {
        $where = Util::getMore([
            ['start_time', ''],
            ['end_time', ''],
            ['nickname', ''],
            ['limit', 20],
            ['page', 1],
            ['type', ''],
        ]);
        return Json::successlayui(FinanceModel::getBillList($where));
    }

    /**
     *保存资金监控的excel表格
     */
    public function save_bell_export()
    {
        $where = Util::getMore([
            ['start_time', ''],
            ['end_time', ''],
            ['nickname', ''],
            ['type', ''],
        ]);
        FinanceModel::SaveExport($where);
    }

    /**
     * 显示操作记录
     */
    public function index3()
    {

    }

}

