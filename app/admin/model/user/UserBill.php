<?php
namespace app\admin\model\user;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use think\facade\Db;

/**
 * 用户消费新增金额明细 model
 * Class User
 * @package app\admin\model\user
 */
class UserBill extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'user_bill';

    use ModelTrait;

    //修改积分减少积分记录
    public static function expend($title,$uid,$category,$type,$number,$link_id = 0,$balance = 0,$mark = '',$status = 1)
    {
        $pm = 0;
        $add_time = time();
        return self::create(compact('title','uid','link_id','category','type','number','balance','mark','status','pm','add_time'));
    }
    //修改积分增加积分记录
    public static function income($title,$uid,$category,$type,$number,$link_id = 0,$balance = 0,$mark = '',$status = 1){
        $pm = 1;
        $add_time = time();
        return self::create(compact('title','uid','link_id','category','type','number','balance','mark','status','pm','add_time'));
    }


    //查询积分个人明细
    public static function getOneIntegralList($where){
        return self::setWhereList(
            $where,'',
//            ['deduction','system_add','sign'],
            ['title','number','balance','mark','FROM_UNIXTIME(add_time,"%Y-%m-%d") as add_time'],
            'integral'
        );
    }
    //查询个人签到明细
    public static function getOneSignList($where){
        return self::setWhereList(
            $where,'sign',
            ['title','number','mark','FROM_UNIXTIME(add_time,"%Y-%m-%d") as add_time']
            );
    }
    //查询个人余额变动记录
    public static function getOneBalanceChangList($where){
         $list=self::setWhereList(
            $where,'',
//            ['system_add','pay_product','extract','pay_product_refund','system_sub','brokerage','recharge','user_recharge_refund'],
            ['FROM_UNIXTIME(add_time,"%Y-%m-%d") as add_time','title','type','mark','number','balance','pm','status'],
            'now_money'
        );
         foreach ($list as &$item){
            switch ($item['type']){
                case 'system_add':
                    $item['_type']='系统添加';
                    break;
                case 'pay_product':
                    $item['_type']='商品购买';
                    break;
                case 'extract':
                    $item['_type']='提现';
                    break;
                case 'pay_product_refund':
                    $item['_type']='退款';
                    break;
                case 'system_sub':
                    $item['_type']='系统减少';
                    break;
                case 'brokerage':
                    $item['_type']='系统返佣';
                    break;
                case 'recharge':
                    $item['_type']='余额充值';
                    break;
                case 'user_recharge_refund':
                    $item['_type']='系统退款';
                    break;
            }
            $item['_pm']=$item['pm']==1 ? '获得': '支出';
         }
         return $list;
    }
    //设置where条件分页.返回数据
    public static function setWhereList($where,$type='',$field=[],$category='integral'){
        $models=self::where('uid',$where['uid'])
            ->where('category',$category)
            ->page((int)$where['page'],(int)$where['limit'])
            ->order('id','desc')
            ->field($field);
        if(is_array($type)){
            $models=$models->where('type','in',$type);
        }else{
            if(!empty($type))$models=$models->where('type',$type);
        }
        return ($list=$models->select()) && count($list) ? $list->toArray():[];
    }
    //获取积分统计头部信息
    public static function getScoreBadgeList($where){
        return [
            [
                'name'=>'历史总积分',
                'field'=>'个',
                'count'=>self::getModelTime($where,new self())->where('category','integral')->where('type','in','gain,system_sub,deduction,sign')->sum('number'),
                'background_color'=>'layui-bg-blue',
                'col'=>4,
            ],
            [
                'name'=>'已使用积分',
                'field'=>'个',
                'count'=>self::getModelTime($where,new self())->where('category','integral')->where('type','deduction')->sum('number'),
                'background_color'=>'layui-bg-cyan',
                'col'=>4,
            ],
            [
                'name'=>'未使用积分',
                'field'=>'个',
                'count'=>self::getModelTime($where,Db::name('user'))->sum('integral'),
                'background_color'=>'layui-bg-cyan',
                'col'=>4,
            ],
        ];
    }
    //获取积分统计曲线图和柱状图
    public static function getScoreCurve($where){
        //发放积分趋势图
         $list=self::getModelTime($where,self::where('category','integral')
            ->field('FROM_UNIXTIME(add_time,"%Y-%m-%d") as _add_time,sum(number) as sum_number')
            ->group('_add_time')->order('_add_time asc'))->select()->toArray();
         $date=[];
         $zoom='';
         $seriesdata=[];
         foreach ($list as $item){
             $date[]=$item['_add_time'];
             $seriesdata[]=$item['sum_number'];
         }
         unset($item);
         if(count($date)>$where['limit']){
             $zoom=$date[$where['limit']-5];
         }
        //使用积分趋势图
        $deductionlist=self::getModelTime($where,self::where('category','integral')->where('type','deduction')
            ->field('FROM_UNIXTIME(add_time,"%Y-%m-%d") as _add_time,sum(number) as sum_number')
            ->group('_add_time')->order('_add_time asc'))->select()->toArray();
         $deduction_date=[];
         $deduction_zoom='';
         $deduction_seriesdata=[];
         foreach ($deductionlist as $item){
             $deduction_date[]=$item['_add_time'];
             $deduction_seriesdata[]=$item['sum_number'];
         }
         if(count($deductionlist)>$where['limit']){
             $deduction_zoom=$deductionlist[$where['limit']-5];
         }
         return compact('date','seriesdata','zoom','deduction_date','deduction_zoom','deduction_seriesdata');
    }

    /**
     * 用户获得总佣金
     * @return float
     */
    public static function getBrokerageCount()
    {
        return self::where('type', 'brokerage')->where('category', 'now_money')->where('status', 1)->where('pm', 1)->sum('number');
    }
}