<?php
/**
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/11/11
 */

namespace app\admin\model\user;

use app\admin\model\order\StoreOrder;
use app\admin\model\user\UserSuper;								   
use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\admin\model\wechat\WechatUser;
use wanyue\services\PHPExcelService;

/**
 * 用户管理 model
 * Class User
 * @package app\admin\model\user
 */
class User extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'user';

    use ModelTrait;

    /**
     * @param $where
     * @return array
     */
    public static function systemPage($where)
    {
        $model = new self;
        if ($where['status'] != '') $model = $model->where('status', $where['status']);
        if (isset($where['user_type']) && $where['user_type'] != '') $model = $model->where('user_type', $where['user_type']);
        if ($where['nickname'] != '') $model = $model->where('nickname|uid', 'like', "%$where[nickname]%");
        $model = $model->order('uid desc');
        return self::page($model, function ($item) {

        }, $where);
    }

    /*
     * 设置搜索条件
     *
     */
    public static function setWhere($where)
    {
        if ($where['order'] != '') {
            $model = self::order(self::setOrder($where['order']));
        } else {
            $model = self::order('u.uid desc');
        }
        if ($where['user_time_type'] == 'visitno' && $where['user_time'] != '') {
            list($startTime, $endTime) = explode(' - ', $where['user_time']);
            $endTime = strtotime($endTime) + 24 * 3600;
            $model = $model->where("u.last_time < " . strtotime($startTime) . " OR u.last_time > " . $endTime);
        }
        if ($where['user_time_type'] == 'visit' && $where['user_time'] != '') {
            list($startTime, $endTime) = explode(' - ', $where['user_time']);
            $model = $model->where('u.last_time', '>', strtotime($startTime));
            $model = $model->where('u.last_time', '<', strtotime($endTime) + 24 * 3600);
        }
        if ($where['user_time_type'] == 'add_time' && $where['user_time'] != '') {
            list($startTime, $endTime) = explode(' - ', $where['user_time']);
            $model = $model->where('u.add_time', '>', strtotime($startTime));
            $model = $model->where('u.add_time', '<', strtotime($endTime) + 24 * 3600);
        }
        if ($where['pay_count'] !== '') {
            if ($where['pay_count'] == '-1') $model = $model->where('pay_count', 0);
            else $model = $model->where('pay_count', '>', $where['pay_count']);
        }
        if ($where['user_type'] != '') {
            if ($where['user_type'] == 'routine') $model = $model->where('w.routine_openid', 'not null');
            else if ($where['user_type'] == 'wechat') $model = $model->where('w.openid', 'not null');
            else $model = $model->where('u.user_type', $where['user_type']);
        }
        if ($where['country'] != '') {
            if ($where['country'] == 'domestic') $model = $model->where('w.country', '中国');
            else if ($where['country'] == 'abroad') $model = $model->where('w.country', '<>', '中国');
        }
        if ($where['group_id'] !== '') {
            $model = $model->where('group_id', $where['group_id']);
        }
        return $model;
    }

    /**
     * 异步获取当前用户 信息
     * @param $where
     * @return array
     */
    public static function getUserList($where)
    {
        $model = self::setWherePage(self::setWhere($where), $where, ['w.sex', 'w.province', 'w.city', 'u.status'], ['u.nickname', 'u.uid', 'u.phone']);
        $list = $model->alias('u')
            ->join('WechatUser w', 'u.uid=w.uid')
            ->field('u.*,w.country,w.province,w.city,w.sex,w.unionid,w.openid,w.routine_openid,w.groupid,w.tagid_list,w.subscribe,w.subscribe_time')
            ->page((int)$where['page'], (int)$where['limit'])
            ->select()
            ->each(function ($item) {
                $item['add_time'] = date('Y-m-d H:i:s', $item['add_time']);
                if ($item['last_time']) $item['last_time'] = date('Y-m-d H:i:s', $item['last_time']);//最近一次访问日期
                else $item['last_time'] = '无访问';//最近一次访问日期
                self::edit(['pay_count' => StoreOrder::getUserCountPay($item['uid'])], $item['uid']);
                $item['extract_count_price'] = UserExtract::getUserCountPrice($item['uid']);//累计提现

                if($item['user_type']=='Android' || $item['user_type']=='IOS'){

                }else if ($item['openid'] != '' && $item['routine_openid'] != '') {
                    $item['user_type'] = '通用';
                } else if ($item['openid'] == '' && $item['routine_openid'] != '') {
                    $item['user_type'] = '小程序';
                } else if ($item['openid'] != '' && $item['routine_openid'] == '') {
                    $item['user_type'] = '公众号';
                } else if ($item['user_type'] == 'h5') {
                    $item['user_type'] = 'H5';
                }
                else $item['user_type'] = '其他';
                if ($item['sex'] == 1) {
                    $item['sex'] = '男';
                } else if ($item['sex'] == 2) {
                    $item['sex'] = '女';
                } else $item['sex'] = '保密';
                $item['vip_name'] = false;
                $item['account'] = m_s($item['account']);
                $item['phone'] = m_s($item['phone']);
				$issuper = UserSuper::where('uid', $item['uid'])->find();
				$item['issuper']=$issuper?1:0;															 
            });//->toArray();
        $count = self::setWherePage(self::setWhere($where), $where, ['w.sex', 'w.province', 'w.city', 'u.status'], ['u.nickname', 'u.uid'])->alias('u')->join('WechatUser w', 'u.uid=w.uid')->count();
        return ['count' => $count, 'data' => $list];
    }

    /**
     *  修改用户状态
     * @param $uids 用户uid
     * @param $status 修改状态
     * @return array
     */
    public static function destrSyatus($uids, $status)
    {
        if (empty($uids) && !is_array($uids)) return false;
        if ($status == '') return false;
        self::beginTrans();
        try {
            $res = self::where('uid', 'in', $uids)->update(['status' => $status]);
            self::checkTrans($res);
            return true;
        } catch (\Exception $e) {
            self::rollbackTrans();
            return Json::fail($e->getMessage());
        }
    }

    /*
     *  获取某季度,某年某年后的时间戳
     *
     * self::getMonth('n',1) 获取当前季度的上个季度的时间戳
     * self::getMonth('n') 获取当前季度的时间戳
     */
    public static function getMonth($time = '', $ceil = 0)
    {
        if (empty($time)) {
            $firstday = date("Y-m-01", time());
            $lastday = date("Y-m-d", strtotime("$firstday +1 month -1 day"));
        } else if ($time == 'n') {
            if ($ceil != 0)
                $season = ceil(date('n') / 3) - $ceil;
            else
                $season = ceil(date('n') / 3);
            $firstday = date('Y-m-01', mktime(0, 0, 0, ($season - 1) * 3 + 1, 1, date('Y')));
            $lastday = date('Y-m-t', mktime(0, 0, 0, $season * 3, 1, date('Y')));
        } else if ($time == 'y') {
            $firstday = date('Y-01-01');
            $lastday = date('Y-12-31');
        } else if ($time == 'h') {
            $firstday = date('Y-m-d', strtotime('this week +' . $ceil . ' day')) . ' 00:00:00';
            $lastday = date('Y-m-d', strtotime('this week +' . ($ceil + 1) . ' day')) . ' 23:59:59';
        }
        return array($firstday, $lastday);
    }

    public static function getcount()
    {
        return self::count();
    }

    /*
    *获取用户某个时间段的消费信息
    *
    * reutrn Array || number
    */
    public static function consume($where, $status = '', $keep = '')
    {
        $model = new self;
        $user_id = [];
        if (is_array($where)) {
            if ($where['status'] != '') $model = $model->where('status', $where['status']);
            switch ($where['date']) {
                case null:
                case 'today':
                case 'week':
                case 'year':
                    if ($where['date'] == null) {
                        $where['date'] = 'month';
                    }
                    if ($keep) {
                        $model = $model->whereTime('add_time', $where['date'])->whereTime('last_time', $where['date']);
                    } else {
                        $model = $model->whereTime('add_time', $where['date']);
                    }
                    break;
                case 'quarter':
                    $quarter = self::getMonth('n');
                    $startTime = strtotime($quarter[0]);
                    $endTime = strtotime($quarter[1]);
                    if ($keep) {
                        $model = $model->where('add_time', '>', $startTime)->where('add_time', '<', $endTime)->where('last_time', '>', $startTime)->where('last_time', '<', $endTime);
                    } else {
                        $model = $model->where('add_time', '>', $startTime)->where('add_time', '<', $endTime);
                    }
                    break;
                default:
                    //自定义时间
                    if (strstr($where['date'], '-') !== FALSE) {
                        list($startTime, $endTime) = explode('-', $where['date']);
                        $model = $model->where('add_time', '>', strtotime($startTime))->where('add_time', '<', bcadd(strtotime($endTime), 86400, 0));
                    } else {
                        $model = $model->whereTime('add_time', 'month');
                    }
                    break;
            }
        } else {
            if (is_array($status)) {
                $model = $model->where('add_time', '>', $status[0])->where('add_time', '<', $status[1]);
            }
        }
        if ($keep === true) {
            return $model->count();
        }
        if ($status === 'default') {
            return $model->group('from_unixtime(add_time,\'%Y-%m-%d\')')->field('count(uid) num,from_unixtime(add_time,\'%Y-%m-%d\') add_time,uid')->select()->toArray();
        }
        if ($status === 'grouping') {
            return $model->group('user_type')->field('user_type')->select()->toArray();
        }
        $uid = $model->field('uid')->select()->toArray();
        foreach ($uid as $val) {
            $user_id[] = $val['uid'];
        }
        if (empty($user_id)) {
            $user_id = [0];
        }
        if ($status === 'xiaofei') {
            $list = UserBill::where('uid', 'in', $user_id)
                ->group('type')
                ->field('sum(number) as top_number,title')
                ->select()
                ->toArray();
            $series = [
                'name' => isset($list[0]['title']) ? $list[0]['title'] : '',
                'type' => 'pie',
                'radius' => ['40%', '50%'],
                'data' => []
            ];
            foreach ($list as $key => $val) {
                $series['data'][$key]['value'] = $val['top_number'];
                $series['data'][$key]['name'] = $val['title'];
            }
            return $series;
        } else if ($status === 'form') {
            $list = WechatUser::where('uid', 'in', $user_id)->group('city')->field('count(city) as top_city,city')->limit(0, 10)->select()->toArray();
            $count = self::getcount();
            $option = [
                'legend_date' => [],
                'series_date' => []
            ];
            foreach ($list as $key => $val) {
                $num = $count != 0 ? (bcdiv($val['top_city'], $count, 2)) * 100 : 0;
                $t = ['name' => $num . '%  ' . (empty($val['city']) ? '未知' : $val['city']), 'icon' => 'circle'];
                $option['legend_date'][$key] = $t;
                $option['series_date'][$key] = ['value' => $num, 'name' => $t['name']];
            }
            return $option;
        } else {
            $number = UserBill::where('uid', 'in', $user_id)->where('type', 'pay_product')->sum('number');
            return $number;
        }
    }

    /*
     * 获取 用户某个时间段的钱数或者TOP20排行
     *
     * return Array  || number
     */
    public static function getUserSpend($date, $status = '')
    {
        $model = new self();
        $model = $model->alias('A');
        switch ($date) {
            case null:
            case 'today':
            case 'week':
            case 'year':
                if ($date == null) $date = 'month';
                $model = $model->whereTime('A.add_time', $date);
                break;
            case 'quarter':
                list($startTime, $endTime) = User::getMonth('n');
                $model = $model->where('A.add_time', '>', strtotime($startTime));
                $model = $model->where('A.add_time', '<', bcadd(strtotime($endTime), 86400, 0));
                break;
            default:
                list($startTime, $endTime) = explode('-', $date);
                $model = $model->where('A.add_time', '>', strtotime($startTime));
                $model = $model->where('A.add_time', '<', bcadd(strtotime($endTime), 86400, 0));
                break;
        }
        if ($status === true) {
            return $model->join('user_bill B', 'B.uid=A.uid')->where('B.type', 'pay_product')->where('B.pm', 0)->sum('B.number');
        }
        $list = $model->join('user_bill B', 'B.uid=A.uid')
            ->where('B.type', 'pay_product')
            ->where('B.pm', 0)
            ->field('sum(B.number) as totel_number,A.nickname,A.avatar,A.now_money,A.uid,A.add_time')
            ->order('totel_number desc')
            ->limit(0, 20)
            ->select()
            ->toArray();
        if (!isset($list[0]['totel_number'])) {
            $list = [];
        }
        return $list;
    }

    /*
     * 获取 相对于上月或者其他的数据
     *
     * return Array
     */
    public static function getPostNumber($date, $status = false, $field = 'A.add_time', $t = '消费')
    {
        $model = new self();
        if (!$status) $model = $model->alias('A');
        switch ($date) {
            case null:
            case 'today':
            case 'week':
            case 'year':
                if ($date == null) {
                    $date = 'last month';
                    $title = '相比上月用户' . $t . '增长';
                }
                if ($date == 'today') {
                    $date = 'yesterday';
                    $title = '相比昨天用户' . $t . '增长';
                }
                if ($date == 'week') {
                    $date = 'last week';
                    $title = '相比上周用户' . $t . '增长';
                }
                if ($date == 'year') {
                    $date = 'last year';
                    $title = '相比去年用户' . $t . '增长';
                }
                $model = $model->whereTime($field, $date);
                break;
            case 'quarter':
                $title = '相比上季度用户' . $t . '增长';
                list($startTime, $endTime) = User::getMonth('n', 1);
                $model = $model->where($field, '>', $startTime);
                $model = $model->where($field, '<', $endTime);
                break;
            default:
                list($startTime, $endTime) = explode('-', $date);
                $title = '相比' . $startTime . '-' . $endTime . '时间段用户' . $t . '增长';
                $Time = strtotime($endTime) - strtotime($startTime);
                $model = $model->where($field, '>', strtotime($startTime) + $Time);
                $model = $model->where($field, '<', strtotime($endTime) + $Time);
                break;
        }
        if ($status) {
            return [$model->count(), $title];
        }
        $number = $model->join('user_bill B', 'B.uid=A.uid')->where('B.type', 'pay_product')->where('B.pm', 0)->sum('B.number');
        return [$number, $title];
    }


    /**获取用户详细信息
     * @param $uid
     * @return array
     */
    public static function getUserInfos($uid)
    {
        $userInfo = self::where('uid', $uid)->find();
        if (!$userInfo) exception('读取用户信息失败!');
        return $userInfo->toArray();
    }

    //获取某人用户推广信息
    public static function getUserinfo($uid)
    {
        $userinfo = self::where('uid', $uid)->field('nickname,now_money,add_time')->find()->toArray();
        $userinfo['number'] = (float)UserBill::where('category', 'now_money')->where('uid', $uid)->where('pm',1)->where('type', 'brokerage')->sum('number');
        //退款退的佣金 -
        $refund_commission = UserBill::where(['uid' => $uid, 'category' => 'now_money', 'type' => 'brokerage'])
            ->where('pm', 0)
            ->sum('number');
        if ($userinfo['number'] > $refund_commission)
            $userinfo['number'] = bcsub($userinfo['number'],$refund_commission,2);
        else
            $userinfo['number'] = 0;
        return $userinfo;
    }

    //获取某用户的详细信息
    public static function getUserDetailed($uid)
    {
        $key_field = ['real_name', 'phone', 'province', 'city', 'district', 'detail', 'post_code'];
        $Address = ($thisAddress = UserAddress::where('uid', $uid)->where('is_default', 1)->field($key_field)->find()) ?
            $thisAddress :
            UserAddress::where('uid', $uid)->field($key_field)->find();
        $UserInfo = self::get($uid);
        return [
            ['col' => 12, 'name' => '默认收货地址', 'value' => $thisAddress ? '收货人:' . $thisAddress['real_name'] . '邮编:' . $thisAddress['post_code'] . ' 收货人电话:' . $thisAddress['phone'] . ' 地址:' . $thisAddress['province'] . ' ' . $thisAddress['city'] . ' ' . $thisAddress['district'] . ' ' . $thisAddress['detail'] : ''],
//            ['name'=>'微信OpenID','value'=>WechatUser::where('uid', $uid)->value('openid'),'col'=>8],
            ['name' => '手机号码', 'value' => $UserInfo['phone']],
//            ['name'=>'ID','value'=>$uid],
            ['name' => '姓名', 'value' => ''],
            ['name' => '微信昵称', 'value' => $UserInfo['nickname']],
            ['name' => '邮箱', 'value' => ''],
            ['name' => '生日', 'value' => ''],
            ['name' => '积分', 'value' => $UserInfo['integral']],
            ['name' => '账户余额', 'value' => $UserInfo['now_money']],
            ['name' => '提现总金额', 'value' => UserExtract::where('uid', $uid)->where('status', 1)->sum('extract_price')],
        ];
    }

    //获取某用户的订单个数,消费明细
    public static function getHeaderList($uid)
    {
        return [
            [
                'title' => '总计订单',
                'value' => StoreOrder::where('uid', $uid)->count(),
                'key' => '笔',
                'class' => '',
            ],
            [
                'title' => '总消费金额',
                'value' => StoreOrder::where('uid', $uid)->where('paid', 1)->sum('total_price'),
                'key' => '元',
                'class' => '',
            ],
            [
                'title' => '本月订单',
                'value' => StoreOrder::where('uid', $uid)->whereTime('add_time', 'month')->count(),
                'key' => '笔',
                'class' => '',
            ],
            [
                'title' => '本月消费金额',
                'value' => StoreOrder::where('uid', $uid)->where('paid', 1)->whereTime('add_time', 'month')->sum('total_price'),
                'key' => '元',
                'class' => '',
            ]
        ];
    }

    /*
     * 获取 会员 订单个数,积分明细,优惠劵明细
     *
     * $uid 用户id;
     *
     * return array
     */
    public static function getCountInfo($uid)
    {
        $order_count = StoreOrder::where('uid', $uid)->count();
        $integral_count = UserBill::where('uid', $uid)->where('category', 'integral')->where('type', 'in', 'deduction,system_add')->count();
        $sign_count = UserBill::where('type', 'sign')->where('uid', $uid)->where('category', 'integral')->count();
        $balanceChang_count = UserBill::where('category', 'now_money')->where('uid', $uid)
            ->where('type', 'in', 'system_add,pay_product,extract,pay_product_refund,system_sub')
            ->count();
        $coupon_count = 0;
        return compact('order_count', 'integral_count', 'sign_count', 'balanceChang_count', 'coupon_count');
    }


}