<?php
namespace app\admin\model\user;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;

/**
 * 用户等级完成任务记录 model
 */

class UserSuper extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'user_super';

    use ModelTrait;
	public static function setstatus($uid,$status){
		
		$model = new self;
		if($status==0){
			if($model->destroy($uid)){
				return 2;
			}else{
				return 0;
			}			
		}else{
			$data=[
				'uid'=>$uid,
				'addtime'=>time(),
			];
			if($model->insert($data)){
				return 1;
			}else{
				return 0;
			}
		}
	}
}