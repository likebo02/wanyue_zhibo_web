{extend name="public/container"}
{block name="content"}

<div class="layui-fluid">
    <div class="layui-row layui-col-space15"  id="app">
        <!--列表-->
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">列表</div>
                <div class="layui-card-body">
                    <div class="layui-btn-container">
                        <a class="layui-btn layui-btn-sm" href="{:Url('index')}">首页</a>
                    </div>
                    <table class="layui-hide" id="List" lay-filter="List"></table>
                    <script type="text/html" id="thumb">
                        {{# if(d.thumb){ }}
                        <img style="cursor: pointer" lay-event='open_image' src="{{d.thumb}}">
                        {{# }else{ }}
                        暂无图片
                        {{# } }}
                    </script>
                    <script type="text/html" id="act">

<!--                        <button class="layui-btn btn-danger layui-btn-xs" lay-event='delstor'>-->
<!--                            <i class="fa fa-times"></i> 删除-->
<!--                        </button>-->
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{__ADMIN_PATH}js/layuiList.js"></script>
{/block}
{block name="script"}
<script>
    setTimeout(function () {
        $('.alert-info').hide();
    },3000);
    //实例化form
    layList.form.render();
    //加载列表
    layList.tableList('List',"{:Url('getlist')}",function (){
        return [
            {field: 'uid', title: 'UID', width:'4%',align:'center'},
            {field: 'nickname', title: '昵称',align:'center'},
            {field: 'classname', title: '分类',align:'center'},
            {field: 'title', title: '标题',align:'center'},
            {field: 'thumb', title: '封面',templet:'#thumb',align:'center'},
            {field: 'goodsnum', title: '商品数量',align:'center'},
            {field: 'likes', title: '点赞数',align:'center'},
            {field: 'nums', title: '累计人数',align:'center'},
            {field: 'goods', title: '销售商品数',align:'center'},
            {field: 'pull', title: '播流地址',align:'center'},
            {field: 'deviceinfo', title: '设备信息',align:'center'},
            {field: 'right', title: '操作',toolbar:'#act',width:'10%',align:'center'},
        ];
    });


    //点击事件绑定
    layList.tool(function (event,data,obj) {
        switch (event) {
            case 'delstor':

                break;
            case 'open_image':
                $eb.openImage(data.thumb);
                break;
        }
    })
</script>
{/block}
