<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/12/18
 */

namespace app\models\shop;

use wanyue\basic\BaseModel;
use think\facade\Cache;

/**
 * TODO 直播分类Model
 * Class StoreCart
 * @package app\models\store
 */
class ShopApply extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'shop_apply';

    /**
     * 申请
     * */
    public static function setApplay($uid ,$realname, $tel, $cer_no, $cer_f, $cer_b, $cer_h, $business, $license, $other)
    {

        $info=self::where('uid', $uid)->find();
        if($info){
            if($info['status']==0) return self::setErrorInfo('店铺正在申请中');
            if($info['status']==1) return self::setErrorInfo('店铺已审核成功');

            $info->realname=$realname;
            $info->tel=$tel;
            $info->cer_no=$cer_no;
            $info->cer_f=$cer_f;
            $info->cer_b=$cer_b;
            $info->cer_h=$cer_h;
            $info->business=$business;
            $info->license=$license;
            $info->other=$other;
            $info->status=0;
            $info->addtime=time();

            return $info->save();
        }else{
            $status=0;
            $addtime=time();
            return self::create(compact('uid', 'realname', 'tel', 'cer_no', 'cer_f', 'cer_b', 'cer_h', 'business', 'license', 'other', 'status', 'addtime'));
        }

    }

    public static function getInfo($uid )
    {
        return self::where('uid',$uid)->find();

        //$key='shop_apply'.$uid;
        //if (Cache::has($key)) {
        //    return Cache::get($key);
        //} else {
            $info=self::where('uid',$uid)->find();
            if($info){
        //        Cache::set($key, $info, $expire);
            }

            return $info;
        //}
    }


}