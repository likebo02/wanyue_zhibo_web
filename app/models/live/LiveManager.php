<?php

namespace app\models\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\admin\model\user\UserSuper;
use app\models\user\User;
/**
 * Class StoreCategory
 * @package app\admin\model\store
 */
class LiveManager extends BaseModel
{



    /**
     * 模型名称
     * @var string
     */
    protected $name = 'Live_manager';

    use ModelTrait;

    /**
     * 检查权限
     * @param $uid
     * @param $liveuid
     * @return int
     */
    public static function checkmanager($uid,$liveuid)
    {
		if($uid == $liveuid){
			return 50;
		}		
		$issuper = UserSuper::where('uid', $uid)->find();
		if($issuper){
			return 60;
		}
		
		$where=[
			['uid','=',$uid],
			['liveuid','=',$liveuid],
		];
	
		$ismanager=self::where($where)->find();


		if($ismanager){
			return  40;
		}
		return 30;
		
    }
    /**
     * 查询列表
     * @param $uid
     * @param $liveuid
     * @return int
     */
    public static function managelist($uid,$p)
    {
		$p=$p>1?$p:1;
		$num=20;
		$start=($p-1)*$num;
		$listobj = self::where('uid', $uid)->limit($start,$num)->select();
		if($listobj){
			$list=$listobj->toArray();
		}else{
			$list=[];
		}
		foreach($list as $k =>$t){
			$userinfo = User::getUserInfo($t['liveuid']);
			$t['nickname']=$userinfo['nickname'];
			$t['avatar']=$userinfo['avatar'];
			$list[$k]=$t;
		}		
		return $list;
		
    }

    /**
     * 查询列表
     * @param $uid
     * @param $liveuid
     * @return int
     */
    public static function managerlist($liveuid,$p)
    {
		$p=$p>1?$p:1;
		$num=20;
		$start=($p-1)*$num;
		$listobj = self::where('liveuid', $liveuid)->limit($start,$num)->select();
		if($listobj){
			$list=$listobj->toArray();
		}else{
			$list=[];
		}
		foreach($list as $k =>$t){
			$userinfo = User::getUserInfo($t['uid']);
			$t['nickname']=$userinfo['nickname'];
			$t['avatar']=$userinfo['avatar'];
			$list[$k]=$t;
		}		
		return $list;
		
    }	
    /**
     * 查询列表
     * @param $uid
     * @param $liveuid
     * @return int
     */
    public static function managercount($liveuid)
    {

		$count = self::where('liveuid', $liveuid)->count();
		if($count){
			return $count;
		}else{
			return 0;
		}

		
    }		
    /**
     * 删除管理
     * @param $uid
     * @param $liveuid
     * @return bool
     */
    public static function delmanager($liveuid,$touid)
    {
		if($touid == $liveuid){
			return self::setErrorInfo('参数错误');
		}		

		$where=[
			['uid','=',$touid],
			['liveuid','=',$liveuid],
		];
		$ismanager=self::where($where)->delete();

		return true;
	
    }	

}